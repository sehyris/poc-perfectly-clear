//
//  UIImage+Orientation.h
//  Demo
//
//  Created by Sehyris Campos on 7/14/17.
//  Copyright © 2017 Athentech Imaging. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Orientation)

- (UIImage *)fixOrientation;

@end
