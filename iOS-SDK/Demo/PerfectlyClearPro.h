/**
***
***  VERSION 7.3.7.0
***
**/

#ifdef __cplusplus
extern "C" {
#endif

//#ifdef _LINUX
#define BOOL int
#define ULONG unsigned long
#define UINT  unsigned int
#define TRUE  1
#define FALSE 0
//#define NULL  0
//#endif

#define NRRETCODE(x) (x & 0x000000FF)
#define CORERETCODE(x) ((x >> 8) & 0x000000FF)
#define FBRETCODE(x) ((x >> 16) & 0x000000FF)
#define RERETCODE(x) ((x >> 24) & 0x000000FF)

#define PFCINTERNAL void*

// return  TRUE to cancel processing.
//         FALSE to continue.
typedef BOOL (*PFC_PROGRESS)(int iPercent);

typedef enum {
    /**
      24 bit RGB - the format is 24 bits per pixel; 8 bits each are used
      for the red, green, and blue components.
      <table> <tr>
        <td>B<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>R<sub>0</sub></td>
        <td>B<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>R<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat24bppRGB,
    /**
      24 bit RGB - the format is 24 bits per pixel; 8 bits each are used
      for the red, green, and blue components.
      <table> <tr>
        <td>R<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>B<sub>0</sub></td>
        <td>R<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>B<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat24bppBGR,
    /**
      32 bit ARGB - the format is 32 bits per pixel; 8 bits each are used
      for the alpha, red, green, and blue components.
      <table> <tr>
        <td>A<sub>0</sub></td>
        <td>R<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>B<sub>0</sub></td>
        <td>A<sub>1</sub></td>
        <td>R<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>B<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat32bppABGR,
    /**
      48 bit RGB - the format is 48 bits per pixel; 16 bits each are used
      for the red, green, and blue components. The 16 bit word is Little-endian.
      <table> <tr>
        <td>B<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>R<sub>0</sub></td>
        <td>B<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>R<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat48bppRGB,
    /**
      64 bit ARGB - the format is 64 bits per pixel; 16 bits each are used
      for the alpha, red, green, and blue components. The 16 bit word is Little-endian.
      <table> <tr>
        <td>B<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>R<sub>0</sub></td>
        <td>A<sub>0</sub></td>
        <td>B<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>R<sub>1</sub></td>
        <td>A<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat64bppARGB,
    /**
      32 bit ARGB - the format is 32 bits per pixel; 8 bits each are used
      for the alpha, red, green, and blue components.
      <table> <tr>
        <td>B<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>R<sub>0</sub></td>
        <td>A<sub>0</sub></td>
        <td>B<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>R<sub>1</sub></td>
        <td>A<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat32bppARGB, // Requested for V3 use
    /**
      48 bit BGR - the format is 48 bits per pixel; 16 bits each are used
      for the red, green, and blue components. The 16 bit word is Little-endian.
      <table> <tr>
        <td>R<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>B<sub>0</sub></td>
        <td>R<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>B<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat48bppBGR,  // Requested for V3 use
    /**
      64 bit ABGR - the format is 64 bits per pixel; 16 bits each are used
      for the alpha, red, green, and blue components. The 16 bit word is Little-endian.
      <table> <tr>
        <td>R<sub>0</sub></td>
        <td>G<sub>0</sub></td>
        <td>B<sub>0</sub></td>
        <td>A<sub>0</sub></td>
        <td>R<sub>1</sub></td>
        <td>G<sub>1</sub></td>
        <td>B<sub>1</sub></td>
        <td>A<sub>1</sub></td>
        <td>...</td>
      </tr> </table> */
	PFC_PixelFormat64bppABGR  // Requested for V3 use

} PFCPIXELFORMAT;

typedef enum {
	ENGINESTATUS_OK = 0,
	ENGINESTATUS_FB_LIBRARY_LOAD_FAIL = 1,
	ENGINESTATUS_FB_FUNCTION_NOT_FOUND = 2
} PFCENGINESTATUS;

typedef enum {
	CALC_CORE = 1,
	CALC_NR = 2,
	CALC_FB = 4,
	CALC_RE = 8,
	CALC_ALL = 15
} PFCFEATURE;

typedef enum {
	PRESET_BEAUTIFY,
	PRESET_BEAUTIFYPLUS,
    PRESET_DETAILS,
	PRESET_VIVID,
	PRESET_INTELLIGENTAUTO
} PFCPRESETID;

typedef struct {
	int x, y;
}
PFCPOINT;

typedef struct {
	int left, top, width, height;
}
PFCRECT;

typedef struct {
	PFCPOINT leftEye, rightEye;
	PFCRECT  face;
}
PFCFBFACEINFO;

typedef struct _PFCFaceRect{
    PFCRECT rect;
    PFCRECT rcEyeL;
    PFCRECT rcEyeR;
    PFCRECT rcMouth;
    PFCPOINT ptEyeL;
    PFCPOINT ptEyeR;
    int angle;
    int blinkLevel;
    int blinkLevelL;
    int blinkLevelR;
    int childLevel;
    int confidence;
    int smileLevel;
    int yawAngle;
    int FAE[3];
    int widthImage;
    int heightImage;
    struct _PFCFaceRect *pNext;
} PFCFACERECT;
typedef PFCFACERECT* LPPFCFACERECT;

typedef struct {
	PFCINTERNAL pEngine;
	unsigned int status;
} PFCENGINE;

typedef struct {
	int width;
	int height;
	int	stride;
	PFCPIXELFORMAT format;
	unsigned char *data;
} PFCIMAGE;

typedef enum {
	TINTCORRECT_AGGRESSIVE,
	TINTCORRECT_DEFAULT,
	TINTCORRECT_CONSERVATIVE,
	TINTCORRECT_STRONGEST
}
TINTCORRECTION;

typedef enum {
	HIGH_CONTRAST,
	HIGH_DEFINITION
}
CONTRASTMODE;

typedef enum {
	DCF_STANDARD,
	DCF_VIVID
}
DCFMODE;

typedef enum {
	AGGRESSIVENESS_CONSERVATIVE,
	AGGRESSIVENESS_MODERATE,
	AGGRESSIVENESS_AGGRESSIVE
}
AGGRESSIVENESS;

typedef enum {
	BIAS_NONE,
	BIAS_ASIAN_PREFERENCE,
	BIAS_AVERAGE_PREFERENCE,
	BIAS_BRIGHTER_PREFERENCE
}
BIASMODE;

typedef enum {
	PFC_NR_SUCCESS,
	PFC_NR_NOTENABLED,
	PFC_NR_FULLRES_REQUIRED,
	PFC_NR_CANCELLED,
	PFC_NR_ERRBITMAP,
	PFC_NR_ERRSETTINGS,
	PFC_NR_MISC_ERROR,
	PFC_NR_NOTFOUND
} PFCNR_STATUS;

typedef enum {
	PFC_CORE_SUCCESS,
	PFC_CORE_NOTENABLED,
	PFC_CORE_CANCELLED,
	PFC_CORE_NOSOURCEIMAGE,
	PFC_CORE_INSUFFICIENTMEMORY,
    	PFC_CORE_MONOLITHIMAGE,
	PFC_CORE_BELOWMINSIZE
} PFCCORE_STATUS;

typedef enum {
	PFC_FB_SUCCESS,
	PFC_FB_NOTENABLED,
	PFC_FB_WARNING,
	PFC_FB_FULLRES_REQUIRED,
	PFC_FB_CANCELLED,
	PFC_FB_FUNCTION_ERROR,
	PFC_FB_CREATE_ENGINE_FAILED,
	PFC_FB_ANALYSIS_FAILED,
	PFC_FB_NO_CORRECTION,
	PFC_FB_NOT_EXECUTED,
	PFC_FB_NOT_AVAILABLE
} PFCFB_STATUS;

typedef enum {
	PFC_RE_SUCCESS,
	PFC_RE_NOTENABLED,
	PFC_RE_FULLRES_REQUIRED,
	PFC_RE_NOT_FOUND,
	PFC_RE_GEN_ERROR,
	PFC_RE_INVALID_PARAMETER,
	PFC_RE_NO_MEMORY,
	PFC_RE_CANCELLED,
	PFC_RE_NOT_SUPPORTED
} PFCRE_STATUS;

typedef enum {
	APPLY_SUCCESS = 0,
	APPLY_ERROR_PROFILE_MISSING = -1,
	APPLY_ERROR_ENGINE_MISSING = -2,
	APPLY_CANCELLED = -3,
	APPLY_NOSOURCE = -4,
	APPLY_BADFORMAT = -5
} PFCAPPLYSTATUS;

typedef struct {
	BOOL bEnabled;
	BOOL bAbnormalTintRemoval;				// Apply Abnormal Tint removal
	TINTCORRECTION eTintMode;
	float fTintScale;
	int iBlackEnhancement;					// Apply Noise Management
	BOOL bVibrancy;						// Apply True Color Calibration
	int iVibrancy;						// Level of True Color Calibration
	int iStrength;						// 0-150 Strength
	BOOL bContrast;						// Apply Athentech patterned contrast improvement algorithm
	CONTRASTMODE eContrastMode;				// Type of contrast
	int iContrast;						// Level of contrast 0 (weakest) to 100 (strongest)
	BIASMODE eBiasMode;
	float	fBiasScale;
	BOOL bSharpen;						// Apply sharpening filter
	float fSharpenScale;					// Side dimension ratio
	BOOL bUseAutomaticStrengthSelection;			// Use Automatic Strength Selection?
	BOOL bUseFAE;						// Use Face Aware Exposure selection?
	AGGRESSIVENESS eAggressiveness;				// How aggressive? 55 (conservative) to 65 (aggressive)
	int iMaxStrength;					// Maximum Limit
	BOOL bInfrared;						// Apply Infrared correction?
	float fInfrared;					// Ratio of Infrared correction
	BOOL  bLightDiffusion;					// Enable light diffusion?
	float fLightDiffusion;
    	BOOL  bDynamicRange;
	BOOL  bDCF;
	DCFMODE eDCFMode;
	float fDCF;
} PFCCOREPARAM;

typedef struct {
    BOOL bEnabled;
    BOOL bSmooth; int iSmoothLevel; int iSmoothMode; int iSmoothType;
    BOOL bEnlarge; int iEnlargeLevel;
    BOOL bEnhance; int iEnhanceLevel;
    BOOL bEyeCirc; int iEyeCirc;
    BOOL bTeeth; int iTeethLevel;
    BOOL bBlemish; int iBlemish;
    BOOL bSlim; int iSlim;
    BOOL bDeFlash; int iDeFlash;
    BOOL bCatchLight; int iCatchLight; int iCatchLightMode;
    BOOL bSkinToning; int iSkinToning; int iSkinToningMode; int iSkinToningType;
    BOOL bLipSharpen; int iLipSharpen; int iLipSharpenType;
    BOOL bBlush; int iBlush;
} PFCFBPARAM;

typedef struct {
	BOOL 	bEnabled;
	int	iPreset;
	int	iStrengthOffset;
	int	iDetailOffset;
} PFCNRPARAM;

typedef struct {
	BOOL bEnabled;
} PFCREPARAM;

typedef struct {
	PFCCOREPARAM core;
	PFCFBPARAM fb;
	PFCNRPARAM nr;
	PFCREPARAM re;
} PFCPARAM;

typedef struct {
	PFCINTERNAL pPfcParam;
	PFCINTERNAL pNoiseParam;
	PFCINTERNAL pRedEyeParam;
	PFCINTERNAL pSFBParam;
	PFCINTERNAL pInternal;

	PFCCORE_STATUS CORE_Status;
	PFCNR_STATUS NR_Status;
	PFCFB_STATUS FB_Status;
	PFCRE_STATUS RE_Status;

	int Status;
} PFCIMAGEPROFILE;
#define PFCPROFILE PFCIMAGEPROFILE*

int PFC_FAEFaceCount(PFCIMAGEPROFILE *pImageProfile);

LPPFCFACERECT PFC_EnumFAEFaceRect(PFCIMAGEPROFILE *pImageProfile, LPPFCFACERECT p);

bool PFC_HasFaceBeautification(PFCENGINE *pEngine);

int PFC_FBFaceCount(PFCPROFILE pImageProfile);

bool PFC_GetFaceInfo(PFCIMAGEPROFILE *pImageProfile, PFCFBFACEINFO *pFace, int index);

bool PFC_AbnormalTintDetected(PFCIMAGEPROFILE *pImageProfile, TINTCORRECTION eTintMethod);

bool PFC_IsNoiseDetected(PFCIMAGEPROFILE *pImageProfile, int iNoisePreset);

void PFC_SetParam(PFCPARAM &param, PFCPRESETID id=PRESET_INTELLIGENTAUTO);

PFCAPPLYSTATUS PFC_Apply(PFCIMAGE *pImage, PFCENGINE *pEngine, PFCPROFILE pImageProfile, PFCPARAM &param, PFC_PROGRESS progfn=NULL, int iOpacity=100);

PFCAPPLYSTATUS PFC_ApplyLocal(PFCIMAGE *pImage, int xOffset, int yOffset, int widthOrig, int heightOrig, PFCENGINE *pEngine, PFCPROFILE pImageProfile, PFCPARAM &param, int iOpacity=100);

PFCPROFILE PFC_Calc(PFCIMAGE *pImage, PFCIMAGE *pImageds, PFCENGINE* pEngine, unsigned int feature=CALC_ALL, int iISO=-1, char *pCameraModel=NULL, PFCPROFILE pImageProfile=NULL, PFC_PROGRESS progfn=NULL, BOOL bRejectMonolith=TRUE);

PFCENGINE* PFC_CreateEngine();

void PFC_DestroyEngine(PFCENGINE* p);

void PFC_ReleaseProfile(PFCPROFILE pProfile);

int PFC_AutoCorrect(PFCIMAGE *pImage, PFCIMAGE *pImageds, PFCPARAM &param, int iISO=-1, char *pCameraModel=NULL, PFC_PROGRESS progfn=NULL);

int PFC_AutoCorrectPreset(PFCIMAGE *pImage, PFCIMAGE *pImageds, PFCPRESETID id=PRESET_INTELLIGENTAUTO, int iISO=-1, char *pCameraModel=NULL, PFC_PROGRESS progfn=NULL);

#ifdef __cplusplus
}
#endif
