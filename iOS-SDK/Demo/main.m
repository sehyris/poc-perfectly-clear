//
//  main.m
//  Demo
//
//  Created by Samuel Lau on 2015-04-29.
//  Copyright (c) 2015 Athentech Imaging. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PfcAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PfcAppDelegate class]));
    }
}
