//
//  PfcViewController.h
//  Demo
//
//  Created by Samuel Lau on 2015-04-29.
//  Copyright (c) 2015 Athentech Imaging. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PfcViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *clickTestButton;
@property (strong, nonatomic) IBOutlet UILabel *labelStatus;
@property (strong, nonatomic) IBOutlet UIImageView *displayView;
@property (weak, nonatomic) IBOutlet UIImageView *originalView;


@end
