//
//  PfcAppDelegate.h
//  Demo
//
//  Created by Samuel Lau on 2015-04-29.
//  Copyright (c) 2015 Athentech Imaging. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PfcAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
