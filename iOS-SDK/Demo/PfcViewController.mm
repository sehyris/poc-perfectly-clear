//
//  PfcViewController.m
//  Demo
//
//  Created by Samuel Lau on 2015-04-29.
//  Copyright (c) 2015 Athentech Imaging. All rights reserved.
//

#import "PfcViewController.h"
#import "PerfectlyClearPro.h"
#import "UIImage+Orientation.h"

@interface PfcViewController ()

@end

NSString *IMG_NAME = @"0.jpg";

@implementation PfcViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.clickTestButton setTitle:@"Begin test" forState:UIControlStateNormal];
    [self.labelStatus setText:@""];
    self.originalView.image = [UIImage imageNamed:IMG_NAME];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

typedef enum {
    format_RGBA8888,
    format_RGB565
} PixelFormat;


- (IBAction)testButtonClicked:(id)sender {
    UIImage *image = [self adjustPhoto];
    [self.displayView setImage:image];
}

- (UIImage *)adjustPhoto
{
    [self.labelStatus setText:@"Sample 1."];
    
    UIImage *image = [UIImage imageNamed:IMG_NAME];
    image = [image fixOrientation];
    
    self.originalView.image = image;
    if (image == nullptr)
        NSLog(@"Unable to read image.");
    CGImageRef imageRef = [image CGImage];
    if (imageRef == NULL)
        NSLog(@"Unable to get imageRef.");
    
    int w = CGImageGetWidth(imageRef);
    int h = CGImageGetHeight(imageRef);
    int stride = CGImageGetBytesPerRow(imageRef);
    NSLog(@"w %d  h %d   stride %d", w, h, stride);
    
    int dataSize = h * stride;
    Byte *pData = NULL;
    pData = (Byte*)malloc(dataSize);
    
    CFDataRef imageData = CGDataProviderCopyData (CGImageGetDataProvider (image.CGImage));
    const UInt32 *pixels = (const UInt32 *)CFDataGetBytePtr (imageData);
    
    memcpy (pData, pixels, dataSize);
    
    // 1. Create engine
    PFCENGINE *pEngine = PFC_CreateEngine();
    
    // 2. Initialize PFCPARAM
    PFCPARAM param;
    PFC_SetParam(param); // if I don't specify the PFCPRESETID, it will use PRESET_INTELLIGENTAUTO. So, what should I set?

    // PFCCOREPARAM
    param.core.bEnabled=true;
    param.core.bAbnormalTintRemoval=false;
    param.core.eTintMode=TINTCORRECT_STRONGEST;
    param.core.fTintScale=1.50;
    param.core.bVibrancy=true;
    param.core.iVibrancy=200;
    param.core.bUseAutomaticStrengthSelection=false;
    param.core.eAggressiveness=AGGRESSIVENESS_MODERATE;
    param.core.iMaxStrength=100;
    param.core.iStrength=200;
    param.core.bContrast=true;
    param.core.iContrast=100;
    param.core.eContrastMode=HIGH_DEFINITION;
    param.core.iBlackEnhancement=12;
    param.core.eBiasMode=BIAS_AVERAGE_PREFERENCE;
    param.core.fBiasScale=1.00;
    param.core.bSharpen=true;
    param.core.fSharpenScale=2.00;
//    param.core.bSkinTone=true;
//    param.core.fSkinTone=1.00;
    param.core.bDCF=false;
    param.core.eDCFMode=DCF_STANDARD;
    param.core.fDCF=1.00;
    param.core.bLightDiffusion=false;
    param.core.fLightDiffusion=1.00;
    param.core.bUseFAE=true;
    param.core.bDynamicRange=true;
    
    // PFCNRPARAM
    param.nr.bEnabled=false;
    param.nr.iPreset=0;
    param.nr.iStrengthOffset=30;
    param.nr.iDetailOffset=-5;
    
    //iOpacity=100
    // NOTE: iOpacity defines fading applied to Perfectly Clear Core and Noise processing in Workbench
    
    // PFCREPARAM
    param.re.bEnabled=true;
    
    // PFCFBPARAM
    // Using auto config. Parameters are applied as a correction to automatically calculated recommended values for each detected face
    // Parameter value 50 corresponds to a recommended value, values below and over are linearly mapped
    // Exception: iCatchLight uses absolute parameter value
    
    param.fb.bEnabled=true;
    param.fb.bSmooth=true;
    param.fb.iSmoothLevel=100;
//    param.fb.eSmoothMode=SKINMODE_FACE;
//    param.fb.eSmoothType=SKINSMOOTHTYPE_SUBTLE;
    param.fb.bEnlarge=false;
    param.fb.iEnlargeLevel=0;
    param.fb.bEnhance=true;
    param.fb.iEnhanceLevel=100;
    param.fb.bEyeCirc=true;
    param.fb.iEyeCirc=100;
    param.fb.bTeeth=false;
    param.fb.iTeethLevel=0;
    param.fb.bBlemish=false;
    param.fb.iBlemish=0;
    param.fb.bSlim=false;
    param.fb.iSlim=0;
    param.fb.bDeFlash=true;
    param.fb.iDeFlash=100;
    param.fb.bCatchLight=false;
    param.fb.iCatchLight=0;
    param.fb.iCatchLightMode=1;
    param.fb.bSkinToning=false;
    param.fb.iSkinToning=0;
//    param.fb.eSkinToningMode=SKINMODE_FACE;
//    param.fb.eSkinToningType=SKINTONINGTYPE_PALE;
    param.fb.bLipSharpen=false;
    param.fb.iLipSharpen=0;
//    param.fb.eLipSharpenType=LIPSHARPENTYPE_FINE;
    param.fb.bBlush=false;
    param.fb.iBlush=0;
    
    // 3. Fill PFCIMAGE structure
    PFCIMAGE im;
    im.width = w;
    im.height = h;
    im.stride = stride;
    im.format = PFC_PixelFormat32bppABGR;
    im.data = (unsigned char*)pData;
    
    NSDate *methodStart = [NSDate date];
    
    // 4. Analyse picture
    PFCPROFILE pProfile = PFC_Calc(&im, NULL, pEngine, CALC_ALL);
    
    NSLog(@"Calc Status %d", pProfile->Status);
    NSLog(@"NR   status %d", (int)pProfile->NR_Status);
    NSLog(@"CORE status %d", (int)pProfile->CORE_Status);
    NSLog(@"FB   status %d", (int)pProfile->FB_Status);
    NSLog(@"RE   status %d", (int)pProfile->RE_Status);
    
    int iFaceCount = PFC_FBFaceCount(pProfile);
    NSLog(@"face count %d", iFaceCount);
    
    // 5. Apply parameters
    PFCAPPLYSTATUS status = PFC_Apply(&im, pEngine, pProfile, param);
    NSLog(@"PFC_Apply status %d", (int)status);
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"executionTime = %f", executionTime);
    [self.labelStatus setText:[NSString stringWithFormat:@"%.4f", executionTime]];
    
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL,
                                                              pData,
                                                              w * h * 4,
                                                              NULL);
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * w;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef imf = CGImageCreate(w,
                                   h,
                                   bitsPerComponent,
                                   bitsPerPixel,
                                   bytesPerRow,
                                   colorSpaceRef,
                                   bitmapInfo,
                                   provider,NULL,NO,renderingIntent);
    
    UIImage *newImage = [UIImage imageWithCGImage:imf];
    
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpaceRef);
    
    CFRelease(imageData);
    PFC_ReleaseProfile(pProfile);
    PFC_DestroyEngine(pEngine);
    
    //    free(pData);
    CGImageRelease(imf);
    
    return newImage;
}

@end
